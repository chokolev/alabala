package user.negative;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class registerTest {
    private WebDriver driver;

    @BeforeMethod
    public void go() {
        String chromePath = Paths.get("chromedriver.exe").toAbsolutePath().toString();
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }
    @AfterMethod
    public void dead(){
        driver.quit();
    }
    @Test
    public void register(){
        WebElement myAccount = driver.findElement(By.xpath("//span[text()='My Account']"));
        myAccount.click();
        WebElement register = driver.findElement(By.cssSelector("a[href='http://shop.pragmatic.bg/index.php?route=account/register']"));
        register.click();
        WebElement registerContinue = driver.findElement(By.xpath("//input[@value='Continue']"));
        registerContinue.click();
        WebElement message = driver.findElement(By.cssSelector("div[class='alert alert-danger alert-dismissible']"));
        Assert.assertEquals(message.getText(), "Warning: You must agree to the Privacy Policy!", "Tuka ima bug");



    }
}
