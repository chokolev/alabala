package user.positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class registerTest {
    private WebDriver driver;

    @BeforeMethod
    public void go() {
        String chromePath = Paths.get("chromedriver.exe").toAbsolutePath().toString();
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @AfterMethod
    public void dead() {
//        driver.quit();
    }

    @Test
    public void register() {
        WebElement myAccount = driver.findElement(By.xpath("//span[text()='My Account']"));
        myAccount.click();
        WebElement register = driver.findElement(By.cssSelector("a[href='http://shop.pragmatic.bg/index.php?route=account/register']"));
        register.click();
        WebElement firstName = driver.findElement(By.xpath("//input[@name='firstname']"));
        firstName.sendKeys("ivan");
        WebElement lastName = driver.findElement(By.xpath("//input[@name='lastname']"));
        lastName.sendKeys("ivanov");
        WebElement email = driver.findElement(By.xpath("//input[@name='email']"));
        email.sendKeys("bla@bla.com");
        WebElement telephone = driver.findElement(By.xpath("//*[@name='telephone']"));
        telephone.sendKeys("123456789");
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        password.sendKeys("blablabla");
        WebElement passwordConfirm = driver.findElement(By.xpath("//input[@name='confirm']"));
        passwordConfirm.sendKeys("blablabla");
        WebElement radioButtonYes = driver.findElement(By.xpath("//input[@name='newsletter']"));
        if (!radioButtonYes.isSelected()) {
            radioButtonYes.click();
        }
        assertTrue(radioButtonYes.isSelected());
        WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox']"));
        if (!checkbox.isSelected())
            checkbox.click();
        assertTrue(checkbox.isSelected());
        WebElement registerContinue = driver.findElement(By.xpath("//input[@value='Continue']"));
        registerContinue.click();


    }
}
